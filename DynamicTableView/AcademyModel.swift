import UIKit

struct AcademyModel {
    let id: Int
    let name: String
    let image: UIImage
    let description: String
}
